const getAllUsersMiddleware = (req, res, next) => {
    console.log("GET all users middleware");

    next();
}

const createUserMiddleware = (req, res, next) => {
    console.log("POST user middleware");

    next();
}

const getUserByIDMiddleware = (req, res, next) => {
    console.log("GET user by id middleware");

    next();
}

const updateUserMiddleware = (req, res, next) => {
    console.log("PUT user middleware");

    next();
}

const deleteUserMiddleware = (req, res, next) => {
    console.log("DELETE user middleware");

    next();
}

module.exports = {
    getAllUsersMiddleware,
    createUserMiddleware,
    getUserByIDMiddleware,
    updateUserMiddleware,
    deleteUserMiddleware
}
