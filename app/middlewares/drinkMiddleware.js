const getAllDrinkMiddleware = (req, res, next) => {
    console.log("GET all drinks middleware");

    next();
}

const createDrinkMiddleware = (req, res, next) => {
    console.log("POST drink middleware");

    next();
}

const getDrinkByIDMiddleware = (req, res, next) => {
    console.log("GET drink by id middleware");

    next();
}

const updateDrinkMiddleware = (req, res, next) => {
    console.log("PUT drink middleware");

    next();
}

const deleteDrinkMiddleware = (req, res, next) => {
    console.log("DELETE drink middleware");

    next();
}

module.exports = {
    getAllDrinkMiddleware,
    createDrinkMiddleware,
    getDrinkByIDMiddleware,
    updateDrinkMiddleware,
    deleteDrinkMiddleware
}
