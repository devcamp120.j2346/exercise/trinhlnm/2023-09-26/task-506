const express = require("express");

const router = express.Router();

const {
    getAllDrinkMiddleware,
    createDrinkMiddleware,
    getDrinkByIDMiddleware,
    updateDrinkMiddleware,
    deleteDrinkMiddleware
} = require("../middlewares/drinkMiddleware");

router.get("/", getAllDrinkMiddleware, (req, res) => {
    res.json({
        message: "GET all drinks"
    })
});

router.post("/", createDrinkMiddleware, (req, res) => {
    res.json({
        message: "POST drink"
    })
})

router.get("/:drinkId", getDrinkByIDMiddleware, (req, res) => {
    const drinkId = req.params.drinkId;

    res.json({
        message: "GET drink id = " + drinkId
    })
})

router.put("/:drinkId", updateDrinkMiddleware, (req, res) => {
    const drinkId = req.params.drinkId;

    res.json({
        message: "PUT drink id = " + drinkId
    })
})

router.delete("/:drinkId", deleteDrinkMiddleware, (req, res) => {
    const drinkId = req.params.drinkId;

    res.json({
        message: "DELETE drink id = " + drinkId
    })
})

module.exports = router;

