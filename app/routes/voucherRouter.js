const express = require("express");

const router = express.Router();

const {
    getAllVouchersMiddleware,
    createVoucherMiddleware,
    getVoucherByIDMiddleware,
    updateVoucherMiddleware,
    deleteVoucherMiddleware
} = require("../middlewares/voucherMiddleware");

router.get("/", getAllVouchersMiddleware, (req, res) => {
    res.json({
        message: "GET all vouchers"
    })
});

router.post("/", createVoucherMiddleware, (req, res) => {
    res.json({
        message: "POST voucher"
    })
})

router.get("/:voucherId", getVoucherByIDMiddleware, (req, res) => {
    const voucherId = req.params.voucherId;

    res.json({
        message: "GET voucher id = " + voucherId
    })
})

router.put("/:voucherId", updateVoucherMiddleware, (req, res) => {
    const voucherId = req.params.voucherId;

    res.json({
        message: "PUT voucher id = " + voucherId
    })
})

router.delete("/:voucherId", deleteVoucherMiddleware, (req, res) => {
    const voucherId = req.params.voucherId;

    res.json({
        message: "DELETE voucher id = " + voucherId
    })
})

module.exports = router;

