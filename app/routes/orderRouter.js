const express = require("express");

const router = express.Router();

const {
    getAllOrdersMiddleware,
    createOrderMiddleware,
    getOrderByIDMiddleware,
    updateOrderMiddleware,
    deleteOrderMiddleware
} = require("../middlewares/orderMiddleware");

router.get("/", getAllOrdersMiddleware, (req, res) => {
    res.json({
        message: "GET all orders"
    })
});

router.post("/", createOrderMiddleware, (req, res) => {
    res.json({
        message: "POST order"
    })
})

router.get("/:orderId", getOrderByIDMiddleware, (req, res) => {
    const orderId = req.params.orderId;

    res.json({
        message: "GET order id = " + orderId
    })
})

router.put("/:orderId", updateOrderMiddleware, (req, res) => {
    const orderId = req.params.orderId;

    res.json({
        message: "PUT order id = " + orderId
    })
})

router.delete("/:orderId", deleteOrderMiddleware, (req, res) => {
    const orderId = req.params.orderId;

    res.json({
        message: "DELETE order id = " + orderId
    })
})

module.exports = router;

