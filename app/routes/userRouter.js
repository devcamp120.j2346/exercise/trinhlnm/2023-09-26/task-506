const express = require("express");

const router = express.Router();

const {
    getAllUsersMiddleware,
    createUserMiddleware,
    getUserByIDMiddleware,
    updateUserMiddleware,
    deleteUserMiddleware
} = require("../middlewares/userMiddleware");

router.get("/", getAllUsersMiddleware, (req, res) => {
    res.json({
        message: "GET all users"
    })
});

router.post("/", createUserMiddleware, (req, res) => {
    res.json({
        message: "POST user"
    })
})

router.get("/:userId", getUserByIDMiddleware, (req, res) => {
    const userId = req.params.userId;

    res.json({
        message: "GET user id = " + userId
    })
})

router.put("/:userId", updateUserMiddleware, (req, res) => {
    const userId = req.params.userId;

    res.json({
        message: "PUT user id = " + userId
    })
})

router.delete("/:userId", deleteUserMiddleware, (req, res) => {
    const userId = req.params.userId;

    res.json({
        message: "DELETE user id = " + userId
    })
})

module.exports = router;

