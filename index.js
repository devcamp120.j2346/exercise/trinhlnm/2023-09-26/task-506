//B1: Import thư viện express
//import express from express
const express = require('express');

//B2: Khởi tạo app express
const app = new express();

//B3: Khai báo cổng để chạy api
const port = 8000;

const drinkRouter = require("./app/routes/drinkRouter");
const voucherRouter = require("./app/routes/voucherRouter");
const userRouter = require("./app/routes/userRouter");
const orderRouter = require("./app/routes/orderRouter");

//Khai báo các api
app.get('/', (req, res) => {
    let today = new Date();
    let message = `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`;
    //res.send(message);
    res.status(200).json({
        message
    })
})

// Sử dụng router 
app.use("/api/v1/drinks", drinkRouter);
app.use("/api/v1/vouchers", voucherRouter);
app.use("/api/v1/users", userRouter);
app.use("/api/v1/orders", orderRouter);

//B4: Start app
app.listen(port, () => {
    console.log(`app listening on port ${port}`);
})
